/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

import java.util.Scanner;

/**
 *
 * @author werapan
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TableMap map = new TableMap(10, 10);
        Robot robot = new Robot(2, 2, 'x', map,20);
        Tree tree = new Tree (6,6);
        Bomb bomb = new Bomb(5,5);
        map.setRobot(robot);
        map.setBomb(bomb);
        map.setTree(tree);
        map.addObj(new Fuel (8,9,30));
        map.addObj(new Fuel (6,10,20));
        map.addObj(new Fuel (7,15,40));
        
        map.addObj(new Tree (20,30));
        map.addObj(new Tree (19,20));
        map.addObj(new Tree (18,39));
        map.addObj(new Tree (16,50));
        map.addObj(new Tree (15,54));
        map.addObj(new Tree (17,66));
        map.addObj(new Tree (14,40));
        
        while(true) {
            map.showMap();
            // W,a| N,w| E,d| S, s| q: quit
            char direction = inputDirection(sc);
            if(direction=='q') {
                printByeBye();
                break;
            }
            robot.walk(direction);
        }
        
    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner sc) {
        String str = sc.next();
        return str.charAt(0);
    }
}
