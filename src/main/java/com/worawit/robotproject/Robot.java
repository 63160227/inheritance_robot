/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

/**
 *
 * @author werapan
 */
public class Robot extends Obj{

    private TableMap map;
    private int Fuel;

    public Robot(int x, int y, char symbol, TableMap map,int Fuel) {
        super(symbol,x,y);
        this.map = map;
        this.Fuel=Fuel;
    }
    public int getFuel(){
        return Fuel;
    }

    public boolean walk(char direction) {
        switch (direction) {
            case 'N': 
            case 'w':
                if (walkN()) return false;
                break;
            case 'S':
            case 's':
                if (walkS()) return false;
                break;
            case 'E':
            case 'd':
                if (walkE()) return false;

                break;
            case 'W':
            case 'a':
                if (walkW()) return false;
                break;
            default:
                return false;
        }
        checkBomb();
        return true;
    }

    private void checkBomb() {
        if(map.isBomb(x, y)) {
            System.out.println("Founded Bomb!!!! (" + x + ", " + y + ")");
        }
    }
    private boolean canWalk(int x ,int y){
        return map.inMap(x,y) && !map.isTree(x, y) && Fuel > 0;
    }
    
    private boolean walkW() {
        CheckFuel();
        if (canWalk(x - 1, y)) {
            x = x - 1;
            Fuel--;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkE() {
        CheckFuel();
        if (canWalk(x + 1, y)) {
            x = x + 1;
            Fuel--;
        } else {
            return true;
        }
        return false;
    }
    private void CheckFuel(){
        int nn = map.fillFuel(this.x,this.y);{
        if(nn>0){
            Fuel+=nn;
        }
        
    }
    }

    private boolean walkS() {
        CheckFuel();
        if (canWalk(x, y + 1)) {
            y = y + 1;
            Fuel--;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkN() {
        CheckFuel();
        if (map.inMap(x, y - 1)) {
            y = y - 1;
            Fuel--;
        } else {
            return true;
        }
        return false;
    }

}