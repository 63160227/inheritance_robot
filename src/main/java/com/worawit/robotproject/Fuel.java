/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

/**
 *
 * @author a
 */
public class Fuel extends Obj{
    private int volume;
    public Fuel (int x ,int y,int vol){
        super('F',x,y);
        this.volume = vol;
    }
    public int fillFuel() {
        int vol = this.volume;
        this.volume=0;
        symbol='-';
        return vol;
    }
    
}

