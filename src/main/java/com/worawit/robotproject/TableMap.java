/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

/**
 *
 * @author werapan
 */
public class TableMap {

    private int width;
    private int height;
    private Robot robot;
    private Bomb bomb;
    private Obj obb[] = new Obj[100];
    private int Objcount;
    private Tree tree;

    public TableMap(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
        addObj(robot);
    }

    public void showSymbol(int x, int y) {
        char r = '-';
        for (int i = 0; i < Objcount; i++) {
            if (obb[i].isOn(x, y)) {
                r = obb[i].getSymbol();
            }
        }
        System.out.print(r);
    }

    public void addObj(Obj obj) {
        obb[Objcount] = obj;
        Objcount++;
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
        addObj(bomb);
    }

    public void setTree(Tree tree) {
        this.tree = tree;
        addObj(tree);

    }

    public boolean isTree(int x, int y) {
        for (int i = 0; i < Objcount; i++) {
            if (obb[i] instanceof Tree && obb[i].isOn(x, y)) {
                return true;
            }
        }
        return false;
    }

    public void showMap() {
        showTitle();
        System.out.println("robot x:" +robot.getX()+ " Y: " +robot.getY()+ "Fuel:"+robot.getFuel());
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                showSymbol(x, y);
            }
            showNewLine();
        }

    }

    private void showTitle() {
        System.out.println("Map");
    }

    private void showNewLine() {
        System.out.println("");
    }

    private void showCell() {
        System.out.print("-");
    }

    private void showBomb() {
        System.out.print(bomb.getSymbol());
    }

    private void showRobot() {
        System.out.print(robot.getSymbol());
    }

    public boolean inMap(int x, int y) {
        // x -> 0-(width-1), y -> 0-(height-1)
        return (x >= 0 && x < width) && (y >= 0 && y < height);
    }

    public boolean isBomb(int x, int y) {
        return bomb.isOn(x, y);
    }
    public int fillFuel (int x ,int y){
        for (int i=0; i<Objcount; i++){
          if(obb[i] instanceof Fuel && obb [i].isOn(x,y)){
            return ((Fuel)(obb[i])).fillFuel();
        }
        }return 0;
    }

}
